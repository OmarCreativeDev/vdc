# vdc

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.1.

## Prerequisites
1. Install node js `https://nodejs.org/en/download/`
2. Install grunt-cli `npm install -g grunt-cli`
3. Install bower `npm install -g bower`

## Build & development
1. Clone git repo `git clone git@gitlab.interoute.com:savicm10/new-vdc2-ui.git` vdc
2. Navigate to repo `cd vdc`
3. run `npm install`
4. run `bower install`
5. Run `grunt` for building and `grunt serve` for preview.

## Testing
Running `grunt test` will run the unit tests with karma.