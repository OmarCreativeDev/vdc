'use strict';

/**
 * @ngdoc overview
 * @name vdcApp
 * @description
 * # vdcApp
 *
 * Main module of the application.
 */
var vdcApp = angular.module('vdcApp', [
							'ngAnimate',
							'ngCookies',
							'ngResource',
							'ngSanitize',
							'ngTouch',
							'ui.router',
							'pascalprecht.translate',
							'filterFromArray',
							'angularUtils.directives.dirPagination',
							'btford.socket-io'
						]);

vdcApp.config(function($stateProvider, $urlRouterProvider, $translateProvider) {
	// For any unmatched url, redirect to /state1
	$urlRouterProvider.otherwise('/dashboard');

	// Now set up the states
	$stateProvider
		.state('styleGuide', {
			templateUrl: 'views/styleGuide.html',
			url: '/style-guide'
		})
		.state('dashboard', {
			controller: 'DashboardCtrl',
			templateUrl: 'views/dashboard.html',
			url: '/dashboard'
		})
		.state('virtualMachines', {
			controller: 'virtualMachinesListCtrl',
			templateUrl: 'views/virtualMachines/list/virtualMachinesList.html',
			url: '/virtual-machines'
		})
		.state('virtualMachines.details', {
			url: '/{vmIndex}/{vmName}/details'
		})
		.state('virtualMachines.savedSnapshots', {
			templateUrl: 'views/virtualMachines/details/savedSnapshots/savedSnapshotsList.html',
			url: '/{vmIndex}/{vmName}/savedSnapshots'
		})
		.state('virtualMachines.nics', {
			templateUrl: 'views/virtualMachines/details/nics/nicsList.html',
			url: '/{vmIndex}/{vmName}/nics'
		})
		.state('storage', {
			templateUrl: 'views/storage/list/storageList.html',
			url: '/storage'
		})
		.state('storage.details', {
			url: '/{storageIndex}/{storageName}/details'
		})
		.state('virtualMachinesDeployment', {
			controller: 'deploymentCtrl',
			templateUrl: 'views/virtualMachines/deployment.html',
			url: '/deployment.html'
		})
		.state('templates', {
			controller: 'TemplatesCtrl',
			templateUrl: 'views/templates.html',
			url: '/templates'
		})
		.state('vTools', {
			controller: 'VToolsCtrl',
			templateUrl: 'views/vTools.html',
			url: '/v-tools'
		})
		.state('billing', {
			controller: 'BillingCtrl',
			templateUrl: 'views/billing.html',
			url: '/billing'
		})
		.state('events', {
			controller: 'EventsCtrl',
			templateUrl: 'views/events.html',
			url: '/events'
		});

	$translateProvider.useLoader('$translatePartialLoader', {
		urlTemplate: '/i18n/{part}/{lang}.json'
	});

	$translateProvider.use('en');
});