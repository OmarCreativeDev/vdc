'use strict';

/**
 * @ngdoc function
 * @name vdcApp.controller:BillingCtrl
 * @description
 * # BillingCtrl
 * Controller of the vdcApp
 */
vdcApp.controller('BillingCtrl', ['$scope', '$translate', '$translatePartialLoader', function($scope, $translate, $translatePartialLoader) {
	$scope.awesomeThings = [
		'HTML5 Boilerplate',
		'AngularJS',
		'Karma'
	];

	$translatePartialLoader.addPart('billing');
	$translate.refresh();
}]);