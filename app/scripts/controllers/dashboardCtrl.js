'use strict';

/**
 * @ngdoc function
 * @name vdcApp.controller:DashboardCtrl

 * @description
 * # DashboardCtrl
 * Controller of the vdcApp
 */
vdcApp.controller('DashboardCtrl', ['$scope', '$translate', '$translatePartialLoader', function($scope, $translate, $translatePartialLoader) {

	console.log('DashboardCtrl loaded');

	$scope.awesomeThings = [
		'HTML5 Boilerplate',
		'AngularJS',
		'Karma'
	];

	$translatePartialLoader.addPart('dashboard');
	$translate.refresh();
}]);