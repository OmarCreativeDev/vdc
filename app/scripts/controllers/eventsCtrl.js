'use strict';

/**
 * @ngdoc function
 * @name vdcApp.controller:EventsCtrl
 * @description
 * # EventsCtrl
 * Controller of the vdcApp
 */
vdcApp.controller('EventsCtrl', ['$scope', '$translate', '$translatePartialLoader', function($scope, $translate, $translatePartialLoader) {
	$scope.awesomeThings = [
		'HTML5 Boilerplate',
		'AngularJS',
		'Karma'
	];

	$translatePartialLoader.addPart('events');
	$translate.refresh();
}]);