'use strict';

vdcApp.controller('mainCtrl', ['$state', '$translate', '$translatePartialLoader', function($state, $translate, $translatePartialLoader) {

	console.log('MainCtrl loaded');
	var mainCtrl = this;

	mainCtrl.state = $state;
	mainCtrl.appleCount = 10;
	mainCtrl.showSideBarNavigation = true;

	$translatePartialLoader.addPart('common/topNavigation');
	$translatePartialLoader.addPart('common/sideBarNavigation');
	$translatePartialLoader.addPart('styleGuide');
	$translate.refresh();

	// set current language as default language
	mainCtrl.currentLanguage = $translate.proposedLanguage();

	mainCtrl.switchLanguage = function(key) {
		$translate.use(key);
		mainCtrl.currentLanguage = key;
	};

	mainCtrl.lazyLoadExample = function(){
		console.log('mainCtrl.lazyLoadExample fired');
		$translatePartialLoader.addPart('lazyLoadExample');
		$translate.refresh();
	};
}]);