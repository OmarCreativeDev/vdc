'use strict';

vdcApp.controller('storageListCtrl', ['$translate', '$translatePartialLoader', '$timeout', 'storageService', '$state', function($translate, $translatePartialLoader, $timeout, storageService, $state) {
	console.log('storageListCtrl fired');

	var storageListCtrl = this;

	storageListCtrl.state = $state;

	// bind modal to controller
	storageListCtrl.storageService = storageService;

	storageListCtrl.listStorage = function(){
		console.log('listStorage fired');

		if(!storageListCtrl.hasOwnProperty('list')){
			storageListCtrl.storageListLoading = true;

			$timeout(function(){
				storageService.getStorageDisks().then(function(response){
					console.log('listStorage success', response);
					storageListCtrl.storageListLoading = false;

					// store virtual machine data on modal
					storageListCtrl.list = response.data.storage;
				}, function(response){
					console.log('listStorage failed', response);
				});
			}, 2000);
		}
	};

	storageListCtrl.showStorageDiskDetails = function(storageDisk, index) {
		console.log('showStorageDiskDetails fired');

		for (var i = 0; i < storageListCtrl.list.length; i++) {
			if (storageListCtrl.list[i] !== storageDisk){
				storageListCtrl.list[i].showDetails = false;
			}
		}

		if(!storageDisk.showDetails){
			$state.go('storage.details', { storageName: storageDisk.name, storageIndex: index} );
		} else {
			$state.go('storage');
		}

		storageDisk.showDetails = !storageDisk.showDetails;
	};

	storageListCtrl.init = function(){
		$translatePartialLoader.addPart('storage');
		$translate.refresh();

		storageListCtrl.listStorage();
	};

	storageListCtrl.init();
}]);