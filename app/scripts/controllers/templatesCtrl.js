'use strict';

/**
 * @ngdoc function
 * @name vdcApp.controller:TemplatesCtrl
 * @description
 * # TemplatesCtrl
 * Controller of the vdcApp
 */
vdcApp.controller('TemplatesCtrl', ['$scope', '$translate', '$translatePartialLoader', function($scope, $translate, $translatePartialLoader) {
	$scope.awesomeThings = [
		'HTML5 Boilerplate',
		'AngularJS',
		'Karma'
	];

	$translatePartialLoader.addPart('templates');
	$translate.refresh();
}]);