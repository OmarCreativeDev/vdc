'use strict';

/**
 * @ngdoc function
 * @name vdcApp.controller:VToolsCtrl
 * @description
 * # VToolsCtrl
 * Controller of the vdcApp
 */
vdcApp.controller('VToolsCtrl', ['$scope', '$translate', '$translatePartialLoader', function($scope, $translate, $translatePartialLoader) {
	$scope.awesomeThings = [
		'HTML5 Boilerplate',
		'AngularJS',
		'Karma'
	];

	$translatePartialLoader.addPart('vTools');
	$translate.refresh();
}]);