'use strict';

vdcApp.controller('nicsCtrl', ['$timeout', 'virtualMachinesService', 'networksService', function($timeout, virtualMachinesService, networksService) {

	var nicsCtrl = this;
	console.log('nicsCtrl loaded', nicsCtrl);

	nicsCtrl.getNetworks = function(){
		console.log('getNetworks fired');

		if(typeof nicsCtrl.networks === 'undefined'){
			nicsCtrl.networksLoading = true;

			$timeout(function(){
				networksService.getNetworks().then(function(response){
					console.log('getNetworks success', response);
					nicsCtrl.networksLoading = false;
					nicsCtrl.networks = response.data.listnetworksresponse.network;
					console.log(nicsCtrl.networks);
				}, function(response){
					console.log('getNetworks failed', response);
				});
			}, 2000);
		}
	};

	nicsCtrl.prefillSecondaryIP = function(){
		console.log('addPropertyToNic fired');
		var nicsArray = [];

		// push each virtual machine nic object inside array
		for(var i = 0; i < virtualMachinesService.list.length; i++){
			nicsArray.push(virtualMachinesService.list[i].nic);
		}

		for(var y = 0; y < nicsArray.length; y++){
			// create secondary ip address
			for(var z = 0; z < nicsArray[y].length; z++){
				var getIpAddress = nicsArray[y][z].ipaddress,
					getLastDotIndexInIp = getIpAddress.lastIndexOf('.');

				// remove last part of secondary ip address so user can enter
				nicsArray[y][z].addIpToNic = getIpAddress.slice(0, getLastDotIndexInIp) + '.';
			}
		}
	};

	nicsCtrl.addNICToVM = function(virtualMachine, form){
		console.log('addNICToVM fired');

		if(form.$valid){
			virtualMachine.showToolTips = false;
			virtualMachine.addNetworkInProgress = true;

			$timeout(function(){
				virtualMachinesService.addNICToVM(virtualMachine.addNetwork, virtualMachine.id).then(function(response){
					console.log('addNICToVM success', response);
				}, function(response){
					console.log('addNICToVM failed', response);
				});

				virtualMachine.addNetworkInProgress = false;

				// close modal
				angular.element('.modal').modal('hide');

				// clear addNetwork property value
				virtualMachine.addNetwork = '';
			}, 2000);

		} else {
			console.log('showToolTips is false');
			virtualMachine.showToolTips = true;
		}
	};

	nicsCtrl.updateDefaultNicForVM = function(nic, virtualMachine){
		console.log('updateDefaultNicForVM fired');

		nic.makeDefaultNICInProgress = true;

		$timeout(function(){
			virtualMachinesService.updateDefaultNicForVM(nic.id, virtualMachine.id).then(function(response){
				console.log('updateDefaultNicForVM success', response);
				for(var i = 0; i < virtualMachine.nic.length; i++){
					virtualMachine.nic[i].isdefault = false;
				}

				nic.isdefault = true;
			}, function(response){
				console.log('updateDefaultNicForVM failed', response);
			});

			nic.makeDefaultNICInProgress = false;
		}, 2000);
	};

	nicsCtrl.removeNICFromVM = function(nicID, nicIndex, virtualMachine){
		console.log('removeNICFromVM fired');

		virtualMachine.removeNICFromVMInProgress = true;

		$timeout(function(){
			console.log(nicID, virtualMachine);

			virtualMachine.nic.splice(nicIndex, 1);

			virtualMachinesService.removeNICFromVM(nicID, virtualMachine.id).then(function(response){
				console.log('removeNICFromVM success', response);
			}, function(response){
				console.log('removeNICFromVM failed', response);
			});

			virtualMachine.removeNICFromVMInProgress = false;

			// close modal
			angular.element('.modal').modal('hide');
		}, 2000);
	};

	nicsCtrl.enableInternetNIC = function(nicID, vmID){
		console.log('enableInternetNIC fired');

		$timeout(function(){
			console.log(nicID, vmID);
		}, 2000);
	};

	nicsCtrl.disableInternetNIC = function(nicID, vmID){
		console.log('disableInternetNIC fired');

		$timeout(function(){
			console.log(nicID, vmID);
		}, 2000);
	};

	nicsCtrl.addIpToNic = function(nic, form){
		console.log('addIpToNic fired');
		console.log(form);
		console.log(nic);

		if(form.$valid){
			nic.showToolTips = false;
			nic.addIpToNicInProgress = true;

			$timeout(function(){
				console.log(nic.id, nic.addIpToNic);

				virtualMachinesService.addIpToNic(nic.id, nic.addIpToNic).then(function(response){
					console.log('addIpToNic success', response);

					if(typeof nic.secondaryIps === 'undefined'){
						nic.secondaryIps = [];
					}

					nic.secondaryIps.push({
						'id': response.data.addiptonicresponse.id,
						'ipaddress': response.data.addiptonicresponse.ipaddress
					});

					// close modal
					angular.element('.modal').modal('hide');

				}, function(response){
					console.log('addIpToNic failed', response);
				});

				nic.addIpToNicInProgress = false;
			}, 2000);

		} else {
			// console.log('showToolTips is false');
			nic.showToolTips = true;
		}
	};

	nicsCtrl.removeIpFromNic = function(nic, secondaryIpIndex){
		console.log('removeIpFromNic fired');

		$timeout(function(){
			console.log(nic, secondaryIpIndex);

			virtualMachinesService.removeIpFromNic(nic.id).then(function(response){
				console.log('removeIpFromNic success', response);
				nic.secondaryIps.splice(secondaryIpIndex, 1);
				console.log(nic.secondaryIps, secondaryIpIndex);
			}, function(response){
				console.log('removeIpFromNic failed', response);
			});
		}, 2000);
	};

	nicsCtrl.toggleAssignIPCheckbox = function(network){
		console.log('toggleAssignIPCheckbox function fired');

		if(network && network.length){
			angular.element('#assignIPToVMCheckbox.collapse').collapse('show');
		} else {
			angular.element('#assignIPToVMCheckbox.collapse').collapse('hide');
			nicsCtrl.assignIPToVMCheckbox = false;
			nicsCtrl.toggleAssignIPInput();
		}
	};

	nicsCtrl.toggleAssignIPInput = function(){
		console.log('toggleAssignIPInput function fired');
		console.log(nicsCtrl.assignIPToVMCheckbox);

		if(nicsCtrl.assignIPToVMCheckbox){
			angular.element('#assignIPToVMInput.collapse').collapse('show');
		} else {
			angular.element('#assignIPToVMInput.collapse').collapse('hide');
		}
	};

	nicsCtrl.prefillSecondaryIP();
}]);