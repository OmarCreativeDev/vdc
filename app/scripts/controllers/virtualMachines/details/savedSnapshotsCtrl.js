'use strict';

vdcApp.controller('savedSnapshotsCtrl', ['$scope', 'virtualMachinesService', '$timeout', '$stateParams', function($scope, virtualMachinesService, $timeout, $stateParams) {

	console.log('savedSnapshotsCtrl loaded');

	$scope.getSavedSnapshots = function(){
		console.log('getSavedSnapshots fired');
		$scope.getVMIndex = $stateParams.vmIndex;

		console.log($scope.getVMIndex);
		console.log($scope.virtualMachines.list[$scope.getVMIndex]);

		if(typeof $scope.virtualMachines.list[$scope.getVMIndex].loadingSavedSnapshots === 'undefined'){
			$scope.virtualMachines.list[$scope.getVMIndex].loadingSavedSnapshots = true;

			$timeout(function(){
				$scope.virtualMachines.getSavedSnapshotsData($scope.virtualMachines.list[$scope.getVMIndex].id).then(function(response){
					console.log('getSavedSnapshotsData success', response);
					$scope.virtualMachines.list[$scope.getVMIndex].savedSnapshots = response.data.savedSnapshotsResponse;
					$scope.virtualMachines.list[$scope.getVMIndex].loadingSavedSnapshots = false;
				}, function(response){
					console.log('getSavedSnapshotsData failed', response);
				});
			}, 2000);
		}
	};

	$scope.deleteSnapshot = function(snapshotID, snapshotIndex){
		console.log('deleteSnapshot fired');
		console.log('snapshotID=' + snapshotID);
		console.log('snapshotIndex=' + snapshotIndex);

		$timeout(function(){
			$scope.virtualMachines.deleteSnapshot(snapshotID).then(function(response){
				console.log('deleteSnapshot success', response);
				console.log( $scope.virtualMachines.list[$scope.getVMIndex].savedSnapshots );

				$scope.virtualMachines.list[$scope.getVMIndex].savedSnapshots.splice(snapshotIndex, 1);
			}, function(response){
				console.log('deleteSnapshot failed', response);
			});
		}, 2000);
	};

	$scope.revertVMToSnapshot = function(snapshotID, snapshotIndex){
		console.log('revertVMToSnapshot fired');
		console.log('snapshotID=' + snapshotID);
		console.log('snapshotIndex=' + snapshotIndex);

		$timeout(function(){
			$scope.virtualMachines.revertVMToSnapshot(snapshotID).then(function(response){
				console.log('revertVMToSnapshot success', response);
			}, function(response){
				console.log('revertVMToSnapshot failed', response);
			});
		}, 2000);
	};

	$scope.init	= function(){
		$scope.getSavedSnapshots();
	};

	$scope.init();
}]);