'use strict';

vdcApp.controller('viewAllVolumesCtrl', ['$scope', '$timeout', function($scope, $timeout) {

	console.log('viewAllVolumesCtrl loaded');

	$scope.viewAllVolumes = function(virtualMachine){
		console.log('viewAllVolumes fired');

		if(typeof virtualMachine.volumes === 'undefined'){
			virtualMachine.viewAllVolumesLoading = true;

			$timeout(function(){
				$scope.virtualMachines.getAllVolumesData(virtualMachine.id).then(function(response){
					console.log('getAllVolumesData success', response);
					virtualMachine.viewAllVolumesLoading = false;
					virtualMachine.volumes = response.data.listvolumesresponse.volume;
				}, function(response){
					console.log('getAllVolumesData failed', response);
				});
			}, 1000);
		}
	};

	$scope.detachDataDisk = function(virtualMachine){
		console.log('detachDataDisk fired');
		console.log(virtualMachine.volumeIDToDetach);

		$timeout(function(){
			$scope.virtualMachines.detachDataDisk(virtualMachine.volumeIDToDetach).then(function(response){
				console.log('detachDataDisk success', response);
			}, function(response){
				console.log('detachDataDisk failed', response);
			});
		}, 1000);
	};

	$scope.checkAddVolumeFormSections = function(virtualMachine, form){
		if(typeof virtualMachine.addVolume === 'undefined'){
			virtualMachine.addVolume = {};
		}

		var newVolumeObject = virtualMachine.addVolume.newVolume,
			array = [];

		for (var value in newVolumeObject) {
			if(typeof newVolumeObject[value] !== 'undefined'){
				array.push(newVolumeObject[value]);
			}
		}

		if(!array.length){
			virtualMachine.addVolume.allSectionsEmpty = true;
		} else {
			virtualMachine.addVolume.allSectionsEmpty = false;
		}

		$scope.validateAddVolumeFormSection(virtualMachine, form);
	};

	$scope.validateAddVolumeFormSection = function(virtualMachine, form){
		console.log(virtualMachine.addVolume);

		if(typeof virtualMachine.addVolume.sectionInFocus !== 'undefined' && form.volumeName.$valid){
			console.log(virtualMachine.addVolume.sectionInFocus);
			var volumeName = virtualMachine.addVolume.volumeName;
			virtualMachine.addVolume.showToolTips = true;

			switch (virtualMachine.addVolume.sectionInFocus) {
				default:
				case 'createNewVolume':
					if(form.diskSpace.$valid && form.diskOffering.$valid){
						var	diskSpace = virtualMachine.addVolume.newVolume.diskSpace,
							diskOffering = virtualMachine.addVolume.newVolume.diskOffering;

						angular.element('.modal').modal('hide');

						$scope.virtualMachines.addVolumeCreateNew(volumeName, diskSpace, diskOffering).then(function(response){
							console.log('addVolumeCreateNew success', response);
							virtualMachine.addVolumeInProgress = true;

							$timeout(function(){
								virtualMachine.addVolumeInProgress = false;
							}, 1000);

							form.$setPristine();
							virtualMachine.addVolume = {};

						}, function(response){
							console.log('addVolumeCreateNew failed', response);
						});
					}
					break;
				case 'uploadYourOwn':
					if(form.uploadVolumeURL.$valid){
						var uploadVolumeURL = virtualMachine.addVolume.newVolume.uploadVolumeURL;
						angular.element('.modal').modal('hide');

						$scope.virtualMachines.addVolumeUploadYourOwn(volumeName, uploadVolumeURL).then(function(response){
							console.log('addVolumeUploadYourOwn success', response);
							virtualMachine.addVolumeInProgress = true;

							$timeout(function(){
								virtualMachine.addVolumeInProgress = false;
							}, 1000);

							form.$setPristine();
							virtualMachine.addVolume = {};

						}, function(response){
							console.log('addVolumeUploadYourOwn failed', response);
						});
					}
					break;
				case 'selectFromExisting':
					if(form.selectedVolume.$valid){
						var selectedVolume = virtualMachine.addVolume.newVolume.selectedVolume;
						angular.element('.modal').modal('hide');

						$scope.virtualMachines.addVolumeSelectFromExisting(volumeName, selectedVolume).then(function(response){
							console.log('addVolumeSelectFromExisting success', response);
							virtualMachine.addVolumeInProgress = true;

							$timeout(function(){
								virtualMachine.addVolumeInProgress = false;
							}, 1000);

							form.$setPristine();
							virtualMachine.addVolume = {};

						}, function(response){
							console.log('addVolumeSelectFromExisting failed', response);
						});
					}
					break;
			}
		}
	};

}]);