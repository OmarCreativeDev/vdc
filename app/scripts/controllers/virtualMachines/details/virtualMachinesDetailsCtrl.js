'use strict';

vdcApp.controller('virtualMachinesDetailsCtrl', ['$scope', '$timeout', function($scope, $timeout) {

	console.log('virtualMachinesDetailsCtrl loaded');

	$scope.changeVirtualMachineName	= function(virtualMachine, form){
		console.log('changeVirtualMachineName fired');
		console.log(form.$valid);

		if(form.$valid){
			virtualMachine.showToolTips = false;
			virtualMachine.editVirtualMachineName = false;
			virtualMachine.shouldBeOpen = false;
			virtualMachine.name = virtualMachine.newVirtualMachineName;

			$scope.virtualMachines.updateVirtualMachineName(virtualMachine.id, virtualMachine.newVirtualMachineName).then(function(response){
				console.log('updateVirtualMachineName success', response);
				console.log(virtualMachine);
			}, function(response){
				console.log('updateVirtualMachineName failed', response);
			});
		} else {
			console.log('showToolTips is false');
			virtualMachine.showToolTips = true;
		}
	};

	$scope.loadAttachedIsos = function(virtualMachine){
		console.log('loadAttachedIsos fired');
		console.log(virtualMachine);

		if(typeof virtualMachine.attachedIsosArray === 'undefined'){
			virtualMachine.attachedIsoLoading = true;

			$timeout(function(){
				$scope.virtualMachines.getAttachedIsoData().then(function(response){
					console.log('loadAttachedIsos success', response);
					virtualMachine.attachedIsosArray = response.data.isos;
					virtualMachine.attachedIsoLoading = false;
				}, function(response){
					console.log('loadAttachedIsos failed', response);
				});
			}, 2000);
		}
	};

	$scope.changeAttachedIso = function(virtualMachine){
		console.log('changeAttachedIso fired');
		console.log(virtualMachine.attachedIso);

		if(typeof virtualMachine.attachedIso !== 'undefined'){
			virtualMachine.attachediso = virtualMachine.attachedIso;
		}

		virtualMachine.editAttachedIso = false;
	};

	$scope.loadCPUCores = function(virtualMachine){
		console.log('loadCPUCores fired');
		console.log(virtualMachine.CPUCoresArray);

		if(typeof virtualMachine.CPUCoresArray === 'undefined'){
			virtualMachine.CPUCoresLoading = true;

			$timeout(function(){
				$scope.virtualMachines.getCPUCoresData().then(function(response){
					console.log('loadCPUCores success', response);
					virtualMachine.CPUCoresArray = response.data.cpucores;
					virtualMachine.CPUCoresLoading = false;
				}, function(response){
					console.log('loadCPUCores failed', response);
				});
			}, 2000);
		}
	};

	$scope.changeCPUCores = function(virtualMachine){
		console.log('changeCPUCores fired');
		console.log(virtualMachine.CPUCores);

		if(typeof virtualMachine.CPUCores !== 'undefined'){
			console.log('has value');
			virtualMachine.cpunumber = virtualMachine.CPUCores;
		}

		virtualMachine.editCPUCores = false;
	};

	$scope.loadMemory = function(virtualMachine){
		console.log('loadMemory fired');
		console.log(virtualMachine.memoryArray);

		if(typeof virtualMachine.memoryArray === 'undefined'){
			virtualMachine.memoryLoading = true;

			$timeout(function(){
				$scope.virtualMachines.getMemoryData().then(function(response){
					console.log('loadMemory success', response);
					virtualMachine.memoryArray = response.data.memory;
					virtualMachine.memoryLoading = false;
				}, function(response){
					console.log('loadMemory failed', response);
				});
			}, 2000);
		}
	};

	$scope.changeMemory = function(virtualMachine){
		console.log('changeMemory fired');
		console.log(virtualMachine.memoryAmount);

		if(typeof virtualMachine.memoryAmount !== 'undefined'){
			console.log('has value');
			virtualMachine.memory = virtualMachine.memoryAmount;
		}

		virtualMachine.editMemory = false;
	};

	$scope.loadAffinityGroups = function(virtualMachine){
		console.log('loadAffinityGroups fired');
		console.log(virtualMachine.affinityGroupsArray);

		if(typeof virtualMachine.affinityGroupsArray === 'undefined'){
			virtualMachine.affinityGroupsLoading = true;

			$timeout(function(){
				$scope.virtualMachines.getAffinityGroupsData().then(function(response){
					console.log('loadAffinityGroups success', response);
					virtualMachine.affinityGroupsArray = response.data.affinityGroups;
					virtualMachine.affinityGroupsLoading = false;
				}, function(response){
					console.log('loadAffinityGroups failed', response);
				});
			}, 2000);
		}
	};

	$scope.changeAffinityGroup = function(virtualMachine){
		console.log('changeAffinityGroup fired');
		console.log(virtualMachine.affinityGroupName);

		if(typeof virtualMachine.affinityGroupName !== 'undefined'){
			console.log('has value');
			virtualMachine.affinitygroup = virtualMachine.affinityGroupName;
		}

		virtualMachine.editAffinityGroup = false;
	};

	$scope.changePassword = function(virtualMachine){
		console.log('changePassword fired');

		$scope.virtualMachines.resetPassword(virtualMachine.id).then(function(response){
			console.log('resetPassword success', response);
			virtualMachine.username = response.data.resetpasswordresponse.username;
			virtualMachine.password = response.data.resetpasswordresponse.password;
			virtualMachine.showLoginDetails = true;
			console.log(virtualMachine);
		}, function(response){
			console.log('resetPassword failed', response);
		});
	};
}]);