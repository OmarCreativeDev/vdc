'use strict';

vdcApp.controller('copyVirtualMachineCtrl', function() {
	console.log('copyVirtualMachineCtrl loaded');

	var copyVirtualMachineCtrl = this;

	copyVirtualMachineCtrl.europeLocations = [
		{
			name: 'Slough',
			countryCode: 'gb'
		}, {
			name: 'London',
			countryCode: 'gb'
		}, {
			name: 'Amsterdam',
			countryCode: 'gb'
		}, {
			name: 'Paris',
			countryCode: 'fr'
		}, {
			name: 'Milan',
			countryCode: 'it'
		}, {
			name: 'Madrid',
			countryCode: 'es'
		}, {
			name: 'Geneva',
			countryCode: 'ch'
		}, {
			name: 'Frankfurt',
			countryCode: 'de'
		}, {
			name: 'Berlin',
			countryCode: 'de'
		}
	];

	copyVirtualMachineCtrl.northAmericaLocations = [
		{
			name: 'Los Angeles',
			countryCode: 'us'
		}, {
			name: 'New York',
			countryCode: 'us'
		}, {
			name: 'Chicago',
			countryCode: 'us'
		}
	];

	copyVirtualMachineCtrl.asiaLocations = [
		{
			name: 'Hong Kong',
			countryCode: 'hk'
		}, {
			name: 'Singapore',
			countryCode: 'sg'
		}
	];

	copyVirtualMachineCtrl.closeLocationPicker = function() {
		console.log('copyVirtualMachineCtrl.closeLocationPicker fired');
		angular.element('#locationPicker').collapse('hide');
		copyVirtualMachineCtrl.locationSelected = true;
	};
});