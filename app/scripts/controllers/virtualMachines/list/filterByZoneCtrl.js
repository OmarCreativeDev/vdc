'use strict';

vdcApp.controller('filterByZoneCtrl', ['$scope', '$filter', '$timeout', function($scope, $filter, $timeout) {

	console.log('filterByZoneCtrl loaded');

	$scope.getFilterByZonesData = function(){
		console.log('$scope.getFilterByZonesData fired');

		$timeout(function(){
			$scope.virtualMachines.getFilterByZonesData().then(function(response){
				$scope.zones = response.data.zones;
				$scope.getAllLocations();
			});
		}, 2000);
	};

	$scope.getAllLocations = function(){
		console.log('$scope.getAllLocations fired');
		$scope.totalLocations = [];

		for(var i = 0; i < $scope.zones.length; i++){
			// store zone object in var
			var zone = $scope.zones[i];

			for(var k = 0; k < zone.locations.length; k++){
				$scope.totalLocations.push(
					zone.locations[k]
				);
			}
		}
	};

	$scope.countCheckedLocations = function(){
		console.log('$scope.countCheckedLocations fired');

		// store checked locations in array
		$scope.checkedLocations = [];

		// if location is checked add it to array
		for(var i = 0; i < $scope.totalLocations.length; i++){
			if ($scope.totalLocations[i].checked){
				$scope.checkedLocations.push($scope.totalLocations[i].name);
			}
		}

		// if all location's are checked check 'selectAllLocations' checkbox
		if ($scope.checkedLocations.length === $scope.totalLocations.length) {
			$scope.selectAllLocations = true;
		} else {
			$scope.selectAllLocations = false;
		}
	};

	$scope.toggleAllLocations = function(){
		console.log('$scope.toggleAllLocations fired');
		console.log($scope.selectAllLocations);

		for(var i = 0; i < $scope.totalLocations.length; i++){
			$scope.totalLocations[i].checked = $scope.selectAllLocations;
		}

		$scope.countCheckedLocations();
	};

	$scope.filterByZone = function(){
		// console.log('filterByZone fired');

		// check and handle if checkedLocations array exists
		if(typeof $scope.checkedLocations === 'undefined') {
			$scope.checkedLocations = [];
		}

		// update modal
		$scope.virtualMachines.filteredVirtualMachineZones = $scope.checkedLocations;

		console.log($scope.checkedLocations);

		if($scope.checkedLocations.length){
			// apply vm zones filtering
			$scope.virtualMachines.list = $filter('filterFromArray')($scope.virtualMachines.originalList, 'zonename', $scope.checkedLocations);

			// apply additonal vm status filtering
			if($scope.virtualMachines.filteredVirtualMachineStatus.length){
				$scope.virtualMachines.list = $filter('filterFromArray')($scope.virtualMachines.list, 'state', $scope.virtualMachines.filteredVirtualMachineStatus);
			}
		}
		else {
			// reset vm data
			$scope.virtualMachines.list = $scope.virtualMachines.originalList;

			// apply vm status filtering
			if($scope.virtualMachines.filteredVirtualMachineStatus.length){
				$scope.virtualMachines.list = $filter('filterFromArray')($scope.virtualMachines.originalList, 'state', $scope.virtualMachines.filteredVirtualMachineStatus);
			}
		}

		console.log($scope.virtualMachines.list.length);
		console.log($scope.virtualMachines.filteredVirtualMachineStatus);
		console.log($scope.virtualMachines.filteredVirtualMachineZones);

		// close modal
		angular.element('.modal').modal('hide');
	};

	$scope.init = function(){
		$scope.getFilterByZonesData();
	};

	$scope.init();

}]);