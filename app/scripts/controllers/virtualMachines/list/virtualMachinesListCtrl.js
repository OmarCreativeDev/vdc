'use strict';

vdcApp.controller('virtualMachinesListCtrl', ['$scope', '$state', 'virtualMachinesService', 'quickActionsService', '$filter', '$timeout', function($scope, $state, virtualMachinesService, quickActionsService, $filter, $timeout) {

	console.log('virtualMachinesListCtrl loaded');

	$scope.showVMQuickActions = false;
	$scope.checkedStatuses = [];
	$scope.$state = $state;

	// bind modals to scope
	$scope.quickActions = quickActionsService;
	$scope.virtualMachines = virtualMachinesService;
	$scope.virtualMachines.allVMsChecked = false;

	$scope.setQuickActionsVirtualMachine = function(virtualMachine){
		console.log('setQuickActionsVirtualMachine fired');

		$scope.virtualMachines.resetCheckedVirtualMachinesInView();
		virtualMachine.checked = true;
		$scope.virtualMachines.countCheckedVirtualMachinesInView();
	};

	$scope.toggleAllVMCheckboxes = function(){
		console.log('toggleAllVMCheckboxes fired');

		for(var i = 0; i < $scope.virtualMachines.virtualMachinesInView.length; i++){
			var vm = $scope.virtualMachines.virtualMachinesInView[i];

			if(typeof vm.operationInProgress === 'undefined' || typeof vm.scheduledOperation === 'undefined'){
				vm.operationInProgress = vm.scheduledOperation = '';
			}

			if(!vm.operationInProgress.length && !vm.scheduledOperation.length){
				vm.checked = $scope.virtualMachines.allVMsChecked;
			}
		}

		$scope.virtualMachines.countCheckedVirtualMachinesInView();
		console.log($scope.virtualMachines.virtualMachinesInView);
	};

	$scope.showVirtualMachineDetails = function(virtualMachine, index) {
		console.log('showVirtualMachineDetails fired');

		for (var i = 0; i < $scope.virtualMachines.list.length; i++) {
			$scope.virtualMachines.list[i].editVirtualMachineName = false;

			if ($scope.virtualMachines.list[i] !== virtualMachine){
				$scope.virtualMachines.list[i].showDetails = false;
				$scope.virtualMachines.list[i].shouldBeOpen = false;
			}
		}

		if(!virtualMachine.showDetails){
			$state.go('virtualMachines.details', { vmName: virtualMachine.name, vmIndex: index});
		} else {
			$state.go('virtualMachines');
		}

		virtualMachine.showDetails = !virtualMachine.showDetails;
	};

	$scope.listVirtualMachines = function(){
		if(!$scope.virtualMachines.hasOwnProperty('list')){
			$scope.VMListLoading = true;

			$timeout(function(){
				$scope.virtualMachines.getVirtualMachines(1).then(function(response){
					console.log('listVirtualMachines success', response);
					$scope.VMListLoading = false;

					// store virtual machine data on modal
					$scope.virtualMachines.list = $scope.virtualMachines.originalList = response.data.virtualmachine;

					$scope.virtualMachines.countVirtualMachinesInView();
				}, function(response){
					console.log('listVirtualMachines failed', response);
				});
			}, 2000);
		}
	};

	$scope.filterByVirtualMachineStatus = function(status){
		// console.log('filterByVirtualMachineStatus fired');

		// add/remove vm status to checkedStatuses array
		if($scope.checkedStatuses.indexOf(status) > -1){
			var index = $scope.checkedStatuses.indexOf(status);
			$scope.checkedStatuses.splice(index, 1);
		} else {
			$scope.checkedStatuses.push(status);
		}

		// update modal
		$scope.virtualMachines.filteredVirtualMachineStatus = $scope.checkedStatuses;

		if($scope.checkedStatuses.length){
			// apply vm status filtering
			$scope.virtualMachines.list = $filter('filterFromArray')($scope.virtualMachines.originalList, 'state', $scope.checkedStatuses);

			// apply additonal vm zones filtering
			if($scope.virtualMachines.filteredVirtualMachineZones.length){
				$scope.virtualMachines.list = $filter('filterFromArray')($scope.virtualMachines.list, 'zonename', $scope.virtualMachines.filteredVirtualMachineZones);
			}
		}
		else {
			// reset vm data
			$scope.virtualMachines.list = $scope.virtualMachines.originalList;

			// apply vm zones filtering
			if($scope.virtualMachines.filteredVirtualMachineZones.length){
				$scope.virtualMachines.list = $filter('filterFromArray')($scope.virtualMachines.originalList, 'zonename', $scope.virtualMachines.filteredVirtualMachineZones);
			}
		}

		console.log($scope.virtualMachines.list.length);
		console.log($scope.virtualMachines.filteredVirtualMachineStatus);
		console.log($scope.virtualMachines.filteredVirtualMachineZones);

		$scope.virtualMachines.resetCheckedVirtualMachinesInView();
	};

	$scope.orderVirtualMachines	= function(sortByProperty, reverse){
		console.log('orderVirtualMachines fired');
		console.log(sortByProperty, reverse);

		if(sortByProperty === 'name'){
			$scope.alphabeticalNames = !$scope.alphabeticalNames;
			$scope.alphabeticalZones = '';
		} else {
			$scope.alphabeticalZones = !$scope.alphabeticalZones;
			$scope.alphabeticalNames = '';
		}

		$scope.virtualMachines.list = $filter('orderBy')($scope.virtualMachines.list, sortByProperty, reverse);
		console.log('re ordered vms', $scope.virtualMachines);

		$scope.virtualMachines.resetCheckedVirtualMachinesInView();
	};

	$scope.pageChanged = function(newPageNumber){
		console.log('pageChanged fired');
		$scope.virtualMachines.pageNumber = newPageNumber;
		$scope.virtualMachines.resetCheckedVirtualMachinesInView();
	};

	$scope.removeQueuedVirtualMachine = function(virtualMachine){
		console.log('removeQueuedVirtualMachine fired');

		virtualMachine.scheduledOperation = '';

		// remove snapshot from scheduled snapshots array
		for(var i = 0; i < $scope.quickActions.queuedVirtualMachines.length; i++) {
			var obj = $scope.quickActions.queuedVirtualMachines[i];

			if(obj.id === virtualMachine.id) {
				$scope.quickActions.queuedVirtualMachines.splice(i, 1);
			}
		}

		console.log('$scope.quickActions.queuedVirtualMachines', $scope.quickActions.queuedVirtualMachines);
	};

	$scope.init	= function(){
		$scope.listVirtualMachines();
	};

	$scope.init();

}]);