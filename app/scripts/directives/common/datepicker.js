vdcApp.directive('datepicker', function() {
	'use strict';
	return {
		restrict: 'A', //E = element, A = attribute, C = class, M = comment
		link: function (scope, element) {
			console.log('datepicker loaded');
			angular.element(element).datepicker({
				dateFormat: 'dd/mm/yy'
			});
		}
	};
});