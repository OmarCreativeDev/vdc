vdcApp.directive('focusMe', function($timeout, $parse) {
	'use strict';
	return {
		link: function(scope, element, attrs) {
			var model = $parse(attrs.focusMe);

			scope.$watch(model, function(value) {
				// console.log('value=', value);
				if (value === true) {
					$timeout(function() {
						element[0].focus();
						element[0].select();
					});
				}
			});
			element.bind('blur', function() {
				// console.log('blur');
				scope.$apply(model.assign(scope, false));
			});
		}
	};
});