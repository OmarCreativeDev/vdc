vdcApp.directive('timepicker', function() {
	'use strict';
	return {
		restrict: 'A', //E = element, A = attribute, C = class, M = comment
		link: function (scope, element) {
			console.log('timepicker loaded');
			angular.element(element).timepicker();
		}
	};
});