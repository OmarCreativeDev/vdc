angular.module('filterFromArray', [])
	.filter('filterFromArray', function() {
		'use strict';

		return function(inputArray, inputProperty, arrayOfStrings) {
			return inputArray.filter( function( el ) {
				return arrayOfStrings.indexOf( el[inputProperty] ) > -1;
			});
		};
	});