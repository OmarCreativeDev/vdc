'use strict';

vdcApp.service('networksService', ['$http', '$q', function($http, $q) {

	console.log('networksService loaded');

	var networksService = this;

	networksService.getNetworks = function() {
		console.log('networksService.getNetworks() called');

		var deferred = $q.defer();

		$http.get('../mocks/networks/listNetworksData.json', { cache: 'true' })
			.then(function (response) {
				deferred.resolve(response);
			});

		return deferred.promise;
	};
}]);