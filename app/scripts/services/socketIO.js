'use strict';

vdcApp.service('socketIO', function(socketFactory){
  	return socketFactory();
});