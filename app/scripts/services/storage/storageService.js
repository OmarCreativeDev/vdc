'use strict';

vdcApp.service('storageService', ['$http', '$q', function($http, $q) {

	console.log('storageService loaded');

	var storageService = this;

	// pagination
	storageService.pageNumber = 1;
	storageService.vmsPerPage = 10;

	storageService.getStorageDisks = function() {
		console.log('storageService.getNetworks() called');

		var deferred = $q.defer();

		$http.get('../mocks/storage/listStorageData.json', { cache: 'true' })
			.then(function (response) {
				deferred.resolve(response);
			});

		return deferred.promise;
	};
}]);