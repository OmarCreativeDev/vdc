'use strict';

vdcApp.service('quickActionsService', ['$filter', '$http', '$q', '$timeout', 'virtualMachinesService', function($filter, $http, $q, $timeout, virtualMachinesService) {

	var quickActionsService = this;
	console.log('quickActionsService loaded', quickActionsService);

	quickActionsService.filterVirtualMachinesActions = function(action){
		console.log('filterVirtualMachinesActions fired');
		console.log(action);

		// filter vm's based on action
		switch(action){
			case 'start':
				virtualMachinesService.checkedVirtualMachines = $filter('filterFromArray')(virtualMachinesService.checkedVirtualMachines, 'state', ['Stopped']);
				break;
			case 'stop':
				virtualMachinesService.checkedVirtualMachines = $filter('filterFromArray')(virtualMachinesService.checkedVirtualMachines, 'state', ['Running']);
				break;
			case 'reboot':
				virtualMachinesService.checkedVirtualMachines = $filter('filterFromArray')(virtualMachinesService.checkedVirtualMachines, 'state', ['Running']);
				break;
			case 'delete':
				virtualMachinesService.checkedVirtualMachines = virtualMachinesService.checkedVirtualMachines;
				break;
			case 'restore':
				virtualMachinesService.checkedVirtualMachines = $filter('filterFromArray')(virtualMachinesService.checkedVirtualMachines, 'state', ['Destroyed']);
				break;
			case 'copy':
				virtualMachinesService.checkedVirtualMachines = $filter('filterFromArray')(virtualMachinesService.checkedVirtualMachines, 'state', ['Running', 'Stopped']);
				break;
			case 'snapshot':
				virtualMachinesService.checkedVirtualMachines = $filter('filterFromArray')(virtualMachinesService.checkedVirtualMachines, 'state', ['Running', 'Stopped']);
				break;
			default:
		}

		// set queuedVirtualMachines array on modal if there are no queued vm's
		if(typeof quickActionsService.queuedVirtualMachines === 'undefined' || !quickActionsService.queuedVirtualMachines.length){
			quickActionsService.queuedVirtualMachines = virtualMachinesService.checkedVirtualMachines;

			// add schedule operation to queued vm's
			for(var i = 0; i < quickActionsService.queuedVirtualMachines.length; i++){
				quickActionsService.queuedVirtualMachines[i].scheduledOperation = action;
			}
		} else {
			// add schedule operation to queued vm's
			for(var y = 0; y < virtualMachinesService.checkedVirtualMachines.length; y++){
				virtualMachinesService.checkedVirtualMachines[y].scheduledOperation = action;
				quickActionsService.queuedVirtualMachines.push(virtualMachinesService.checkedVirtualMachines[y]);
			}
		}

		// uncheck all vm's
		virtualMachinesService.resetCheckedVirtualMachinesInView();

		console.log(virtualMachinesService.checkedVirtualMachines);

		// close modal
		angular.element('.modal').modal('hide');

		quickActionsService.actionQueuedVirtualMachine();
	};

	quickActionsService.actionQueuedVirtualMachine = function(){
		console.log('actionQueuedVirtualMachine fired');

		// set first vm from scheduled to in progress
		quickActionsService.queuedVirtualMachines[0].operationInProgress = quickActionsService.queuedVirtualMachines[0].scheduledOperation;
		quickActionsService.queuedVirtualMachines[0].scheduledOperation = '';

		switch(quickActionsService.queuedVirtualMachines[0].operationInProgress) {
			case 'start':
				quickActionsService.startVirtualMachine();
				break;
			case 'stop':
				quickActionsService.stopVirtualMachine();
				break;
			case 'reboot':
				quickActionsService.rebootVirtualMachine();
				break;
			case 'delete':
				quickActionsService.deleteVirtualMachine();
				break;
			case 'restore':
				quickActionsService.restoreVirtualMachine();
				break;
			case 'copy':
				quickActionsService.copyVirtualMachine();
				break;
			case 'snapshot':
				quickActionsService.takeVirtualMachineSnapshot();
				break;
			default:
		}
	};

	quickActionsService.startVirtualMachine = function(){
		console.log('%c startVirtualMachine fired', 'color:white; font-weight:bold; background:green');
		console.log('%c quickActionsService.queuedVirtualMachines.length = ' + quickActionsService.queuedVirtualMachines.length, 'color:black; font-weight:bold; background:yellow');

		$timeout(function(){
			virtualMachinesService.startVirtualMachine(quickActionsService.queuedVirtualMachines[0].id).then(function(response){
				console.log('startVirtualMachine success', response);

				quickActionsService.queuedVirtualMachines[0].operationInProgress = '';

				// update vm status
				quickActionsService.queuedVirtualMachines[0].state = 'Running';

				// remove completed vm from queuedVirtualMachines
				quickActionsService.queuedVirtualMachines.splice(0, 1);

				if(quickActionsService.queuedVirtualMachines.length){
					quickActionsService.actionQueuedVirtualMachine();
				}
			}, function(response){
				console.log('startVirtualMachine failed', response);
			});
		}, 2000);
	};

	quickActionsService.stopVirtualMachine = function(){
		console.log('%c stopVirtualMachine fired', 'color:black; font-weight:bold; background:yellow');
		console.log('%c quickActionsService.queuedVirtualMachines.length = ' + quickActionsService.queuedVirtualMachines.length, 'color:black; font-weight:bold; background:yellow');

		$timeout(function(){
			virtualMachinesService.stopVirtualMachine(quickActionsService.queuedVirtualMachines[0].id).then(function(response){
				console.log('stopVirtualMachine success', response);

				quickActionsService.queuedVirtualMachines[0].operationInProgress = '';

				// update vm status
				quickActionsService.queuedVirtualMachines[0].state = 'Stopped';

				// remove completed vm from queuedVirtualMachines
				quickActionsService.queuedVirtualMachines.splice(0, 1);

				if(quickActionsService.queuedVirtualMachines.length){
					quickActionsService.actionQueuedVirtualMachine();
				}
			}, function(response){
				console.log('stopVirtualMachine failed', response);
			});
		}, 2000);
	};

	quickActionsService.rebootVirtualMachine = function(){
		console.log('%c rebootVirtualMachine fired', 'color:black; font-weight:bold; background:yellow');
		console.log('%c quickActionsService.queuedVirtualMachines.length = ' + quickActionsService.queuedVirtualMachines.length, 'color:black; font-weight:bold; background:yellow');

		$timeout(function(){
			virtualMachinesService.rebootVirtualMachine(quickActionsService.queuedVirtualMachines[0].id).then(function(response){
				console.log('rebootVirtualMachine success', response);

				quickActionsService.queuedVirtualMachines[0].operationInProgress = '';

				// update vm status
				quickActionsService.queuedVirtualMachines[0].state = 'Running';

				// remove completed vm from queuedVirtualMachines
				quickActionsService.queuedVirtualMachines.splice(0, 1);

				if(quickActionsService.queuedVirtualMachines.length){
					quickActionsService.actionQueuedVirtualMachine();
				}
			}, function(response){
				console.log('rebootVirtualMachine failed', response);
			});
		}, 2000);
	};

	quickActionsService.clearDeleteVMOptions = function(){
		console.log('clearDeleteVMOptions fired');

		quickActionsService.expungeVM = false;
		quickActionsService.confirmVMDeletion = '';
	};

	quickActionsService.deleteVirtualMachine = function(){
		console.log('expunge vm = ' + quickActionsService.expungeVM);

		console.log('%c deleteVirtualMachine fired', 'color:white; font-weight:bold; background:blue');
		console.log('%c quickActionsService.queuedVirtualMachines.length = ' + quickActionsService.queuedVirtualMachines.length, 'color:white; font-weight:bold; background:red');

		$timeout(function(){
			virtualMachinesService.destroyVirtualMachine(quickActionsService.queuedVirtualMachines[0].id).then(function(response){
				console.log('destroyVirtualMachine success', response);

				quickActionsService.queuedVirtualMachines[0].operationInProgress = '';

				// remove completed vm from queuedVirtualMachines
				quickActionsService.queuedVirtualMachines.splice(0, 1);

				// delete vm
				virtualMachinesService.list.splice(0, 1);

				// re count vms in view
				virtualMachinesService.countVirtualMachinesInView();

				if(quickActionsService.queuedVirtualMachines.length){
					quickActionsService.actionQueuedVirtualMachine();
				}
			}, function(response){
				console.log('takeSnapshot failed', response);
			});
		}, 2000);
	};

	quickActionsService.restoreVirtualMachine = function(){
		console.log('%c restoreVirtualMachine fired', 'color:black; font-weight:bold; background:green');
		console.log('%c quickActionsService.queuedVirtualMachines.length = ' + quickActionsService.queuedVirtualMachines.length, 'color:black; font-weight:bold; background:yellow');

		$timeout(function(){
			virtualMachinesService.restoreVirtualMachine(quickActionsService.queuedVirtualMachines[0].id).then(function(response){
				console.log('restoreVirtualMachine success', response);

				quickActionsService.queuedVirtualMachines[0].operationInProgress = '';

				// update vm status
				quickActionsService.queuedVirtualMachines[0].state = 'Running';

				// remove completed vm from queuedVirtualMachines
				quickActionsService.queuedVirtualMachines.splice(0, 1);

				if(quickActionsService.queuedVirtualMachines.length){
					quickActionsService.actionQueuedVirtualMachine();
				}
			}, function(response){
				console.log('restoreVirtualMachine failed', response);
			});
		}, 2000);
	};

	quickActionsService.copyVirtualMachine = function(){
		console.log('%c copyVirtualMachine fired', 'color:white; font-weight:bold; background:green');
		console.log('%c quickActionsService.queuedVirtualMachines.length = ' + quickActionsService.queuedVirtualMachines.length, 'color:black; font-weight:bold; background:yellow');

		$timeout(function(){
			virtualMachinesService.copyVirtualMachine(quickActionsService.queuedVirtualMachines[0].id).then(function(response){
				console.log('copyVirtualMachine success', response);

				quickActionsService.queuedVirtualMachines[0].operationInProgress = '';

				// update vm status
				quickActionsService.queuedVirtualMachines[0].state = 'Stopped';

				// add copied vm
				virtualMachinesService.list.unshift(response.data.copyvirtualmachineresponse);

				// remove completed vm from queuedVirtualMachines
				quickActionsService.queuedVirtualMachines.splice(0, 1);

				// re count vms in view
				virtualMachinesService.countVirtualMachinesInView();

				if(quickActionsService.queuedVirtualMachines.length){
					quickActionsService.actionQueuedVirtualMachine();
				}
			}, function(response){
				console.log('copyVirtualMachine failed', response);
			});
		}, 2000);
	};

	quickActionsService.takeVirtualMachineSnapshot = function(){
		console.log('%c takeVirtualMachineSnapshot fired', 'color:white; font-weight:bold; background:black');
		console.log('%c quickActionsService.queuedVirtualMachines.length = ' + quickActionsService.queuedVirtualMachines.length, 'color:white; font-weight:bold; background:red');

		$timeout(function(){
			virtualMachinesService.takeSnapshot(quickActionsService.queuedVirtualMachines[0].id).then(function(response){
				console.log('takeSnapshot success', response);

				quickActionsService.queuedVirtualMachines[0].operationInProgress = '';

				// remove completed vm from queuedVirtualMachines
				quickActionsService.queuedVirtualMachines.splice(0, 1);

				if(quickActionsService.queuedVirtualMachines.length){
					quickActionsService.actionQueuedVirtualMachine();
				}
			}, function(response){
				console.log('takeSnapshot failed', response);
			});
		}, 2000);
	};
}]);