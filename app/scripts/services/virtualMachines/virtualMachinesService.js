'use strict';

vdcApp.service('virtualMachinesService', ['$http', '$q', function($http, $q) {

	var virtualMachinesService = this;
	console.log('virtualMachinesService loaded', virtualMachinesService);

	// pagination
	virtualMachinesService.pageNumber = 1;
	virtualMachinesService.vmsPerPage = 10;

	// store filtered vm status, zones on modal
	virtualMachinesService.filteredVirtualMachineStatus = virtualMachinesService.filteredVirtualMachineZones = [];

	virtualMachinesService.countVirtualMachinesInView = function(){
		virtualMachinesService.firstVMInViewIndex = (virtualMachinesService.pageNumber * virtualMachinesService.vmsPerPage) - virtualMachinesService.vmsPerPage;
		console.log('countVirtualMachinesInView fired');
		console.log('%c firstVMInViewIndex = ' + virtualMachinesService.firstVMInViewIndex, 'color:white; font-weight:bold; background:red');

		virtualMachinesService.virtualMachinesInView = [];

		for(var i = 0; i < virtualMachinesService.vmsPerPage; i++){
			if(typeof virtualMachinesService.list[virtualMachinesService.firstVMInViewIndex] !== 'undefined'){
				virtualMachinesService.virtualMachinesInView.push(virtualMachinesService.list[virtualMachinesService.firstVMInViewIndex]);
				virtualMachinesService.firstVMInViewIndex++;
			}
		}
	};

	virtualMachinesService.resetCheckedVirtualMachinesInView = function(){
		console.log('resetCheckedVirtualMachinesInView fired');
		virtualMachinesService.allVMsChecked = false;

		for(var i = 0; i < virtualMachinesService.list.length; i++){
			virtualMachinesService.list[i].checked = false;
		}

		virtualMachinesService.countVirtualMachinesInView();
	};

	virtualMachinesService.countCheckedVirtualMachinesInView = function(){
		console.log('countCheckedVirtualMachinesInView fired');

		virtualMachinesService.checkedVirtualMachines = [];

		// if vm is checked add it to array
		for(var i = 0; i < virtualMachinesService.list.length; i++){
			if (virtualMachinesService.list[i].checked){
				virtualMachinesService.checkedVirtualMachines.push(virtualMachinesService.list[i]);
			}
		}

		console.log(virtualMachinesService.checkedVirtualMachines);

		// if all vm's are checked check 'allVMsChecked' checkbox
		if (virtualMachinesService.checkedVirtualMachines.length === virtualMachinesService.virtualMachinesInView.length) {
			virtualMachinesService.allVMsChecked = true;
		} else {
			virtualMachinesService.allVMsChecked = false;
		}

		virtualMachinesService.checkedVirtualMachinesStates();
	};

	virtualMachinesService.checkedVirtualMachinesStates = function(){
		console.log('checkedVirtualMachinesStates fired');

		virtualMachinesService.runningVirtualMachines = [];
		virtualMachinesService.stoppedVirtualMachines = [];
		virtualMachinesService.destroyedVirtualMachines = [];

		for(var i = 0; i < virtualMachinesService.checkedVirtualMachines.length; i++){
			if (virtualMachinesService.checkedVirtualMachines[i].state === 'Running'){
				virtualMachinesService.runningVirtualMachines.push(virtualMachinesService.checkedVirtualMachines[i]);
			}
			else if (virtualMachinesService.checkedVirtualMachines[i].state === 'Stopped'){
				virtualMachinesService.stoppedVirtualMachines.push(virtualMachinesService.checkedVirtualMachines[i]);
			}
			else if (virtualMachinesService.checkedVirtualMachines[i].state === 'Destroyed'){
				virtualMachinesService.destroyedVirtualMachines.push(virtualMachinesService.checkedVirtualMachines[i]);
			}
		}

		// set onlyDestroyedVirtualMachinesChecked boolean property
		if(virtualMachinesService.destroyedVirtualMachines.length && virtualMachinesService.destroyedVirtualMachines.length === virtualMachinesService.checkedVirtualMachines.length){
			virtualMachinesService.onlyDestroyedVirtualMachinesChecked = true;
		} else {
			virtualMachinesService.onlyDestroyedVirtualMachinesChecked = false;
		}

		// set onlyStoppedVirtualMachinesChecked boolean property
		if(virtualMachinesService.stoppedVirtualMachines.length && virtualMachinesService.stoppedVirtualMachines.length === virtualMachinesService.checkedVirtualMachines.length){
			virtualMachinesService.onlyStoppedVirtualMachinesChecked = true;
		} else {
			virtualMachinesService.onlyStoppedVirtualMachinesChecked = false;
		}

		// set onlyRunningVirtualMachinesChecked boolean property
		if(virtualMachinesService.runningVirtualMachines.length && virtualMachinesService.runningVirtualMachines.length === virtualMachinesService.checkedVirtualMachines.length){
			virtualMachinesService.onlyRunningVirtualMachinesChecked = true;
		} else {
			virtualMachinesService.onlyRunningVirtualMachinesChecked = false;
		}

		virtualMachinesService.totalRunningAndStoppedVirtualMachines = virtualMachinesService.runningVirtualMachines.length + virtualMachinesService.stoppedVirtualMachines.length;
		virtualMachinesService.totalStoppedAndDestroyedVirtualMachines = virtualMachinesService.stoppedVirtualMachines.length + virtualMachinesService.destroyedVirtualMachines.length;

		console.log('%c virtualMachinesService.runningVirtualMachines.length = ' + virtualMachinesService.runningVirtualMachines.length, 'color:white; font-weight:bold; background:green');
		console.log('%c virtualMachinesService.stoppedVirtualMachines.length = ' + virtualMachinesService.stoppedVirtualMachines.length, 'color:black; font-weight:bold; background:yellow');
		console.log('%c virtualMachinesService.destroyedVirtualMachines.length = ' + virtualMachinesService.destroyedVirtualMachines.length, 'color:white; font-weight:bold; background:red');
	};

	virtualMachinesService.getVirtualMachines = function(pageNumber) {
		console.log('virtualMachinesService.getData() called with pageNumber ' + pageNumber);

		var deferred = $q.defer();

		$http.get('../mocks/virtualMachines/listVirtualMachinesData.json', { cache: 'true' })
			.then(function (response) {
				deferred.resolve(response);
			});

		return deferred.promise;
	};

	virtualMachinesService.getFilterByZonesData = function(){
		console.log('virtualMachinesService.getFilterByZonesData fired');

		var deferred = $q.defer();

		$http.get('../mocks/virtualMachines/listFilterByZonesData.json', { cache: 'true' })
			.then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.updateVirtualMachineName = function(virtualMachineID, newVirtualMachineName) {
		console.log('virtualMachinesService.updateVirtualMachineName() called with ' + virtualMachineID + newVirtualMachineName);

		var deferred = $q.defer();

		$http.get('../mocks/virtualMachines/updatedVirtualMachineResponse.json', { cache: 'true' })
			.then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.getAttachedIsoData = function(){
		console.log('virtualMachinesService.getAttachedIsoData fired');

		var deferred = $q.defer();

		$http.get('../mocks/virtualMachines/listAttachedIsoData.json', { cache: 'true' })
			.then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.getCPUCoresData = function(){
		console.log('virtualMachinesService.getCPUCoresData fired');

		var deferred = $q.defer();

		$http.get('../mocks/virtualMachines/listCPUCoresData.json', { cache: 'true' })
			.then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.getMemoryData = function(){
		console.log('virtualMachinesService.getMemoryData fired');

		var deferred = $q.defer();

		$http.get('../mocks/virtualMachines/listMemoryData.json', { cache: 'true' })
			.then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.getAffinityGroupsData = function(){
		console.log('virtualMachinesService.getAffinityGroupsData fired');

		var deferred = $q.defer();

		$http.get('../mocks/virtualMachines/listAffinityGroupsData.json', { cache: 'true' })
			.then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.getAllVolumesData = function(virtualMachineID){
		console.log('virtualMachinesService.getAllVolumesData fired with ' + virtualMachineID);

		var deferred = $q.defer();

		$http.get('../mocks/virtualMachines/listAllVolumesData.json', { cache: 'true' })
			.then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.detachDataDisk = function(volumeIDToDetach){
		console.log('virtualMachinesService.detachDataDisk fired with ' + volumeIDToDetach);

		var deferred = $q.defer();

		$http.get('../mocks/virtualMachines/detachDataDiskResponse.json', { cache: 'true' })
			.then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.addVolumeCreateNew = function(volumeName, diskSpace, diskOffering){
		console.log('virtualMachinesService.addVolumeCreateNew fired with ' + volumeName, diskSpace, diskOffering);

		var deferred = $q.defer();

		$http({
			url: '../mocks/virtualMachines/addVolumeResponse.json',
			method: 'GET',
			cache: 'true',
			params: {
				volumeName: volumeName,
				diskSpace: diskSpace,
				diskOffering: diskOffering
			}
			}).then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.addVolumeUploadYourOwn = function(volumeName, uploadVolumeURL){
		console.log('virtualMachinesService.addVolumeUploadYourOwn fired with ' + volumeName, uploadVolumeURL);

		var deferred = $q.defer();

		$http({
			url: '../mocks/virtualMachines/addVolumeResponse.json',
			method: 'GET',
			cache: 'true',
			params: {
				volumeName: volumeName,
				uploadVolumeURL: uploadVolumeURL
			}
			}).then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.addVolumeSelectFromExisting = function(volumeName, selectedVolume){
		console.log('virtualMachinesService.addVolumeSelectFromExisting fired with ' + volumeName, selectedVolume);

		var deferred = $q.defer();

		$http({
			url: '../mocks/virtualMachines/addVolumeResponse.json',
			method: 'GET',
			cache: 'true',
			params: {
				volumeName: volumeName,
				selectedVolume: selectedVolume
			}
			}).then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.resetPassword = function(virtualMachineID){
		console.log('virtualMachinesService.resetPassword fired with ' + virtualMachineID);

		var deferred = $q.defer();

		$http({
			url: '../mocks/virtualMachines/resetPasswordResponse.json',
			method: 'GET',
			cache: 'true',
			params: {
				virtualMachineID: virtualMachineID
			}
			}).then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.getSavedSnapshotsData = function(virtualMachineID){
		console.log('virtualMachinesService.getSavedSnapshotsData fired with ' + virtualMachineID);

		var deferred = $q.defer();

		$http({
			url: '../mocks/virtualMachines/savedSnapshotsResponse.json',
			method: 'GET',
			cache: 'true',
			params: {
				virtualMachineID: virtualMachineID
			}
			}).then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.takeSnapshot = function(id) {
		console.log('virtualMachinesService.takeSnapshot() called with id ' + id);

		var deferred = $q.defer();

		$http.get('../mocks/virtualMachines/listTakeSnapshotData.json', { cache: 'true' })
			.then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.rebootVirtualMachine = function(id) {
		console.log('virtualMachinesService.rebootVirtualMachine() called with id ' + id);

		var deferred = $q.defer();

		$http({
			url: '../mocks/virtualMachines/rebootVirtualMachineResponse.json',
			method: 'GET',
			cache: 'true',
			params: {
				id: id
			}
			}).then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.startVirtualMachine = function(id) {
		console.log('virtualMachinesService.startVirtualMachine() called with id ' + id);

		var deferred = $q.defer();

		$http({
			url: '../mocks/virtualMachines/startVirtualMachineResponse.json',
			method: 'GET',
			cache: 'true',
			params: {
				id: id
			}
			}).then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.stopVirtualMachine = function(id) {
		console.log('virtualMachinesService.stopVirtualMachine() called with id ' + id);

		var deferred = $q.defer();

		$http({
			url: '../mocks/virtualMachines/stopVirtualMachineResponse.json',
			method: 'GET',
			cache: 'true',
			params: {
				id: id
			}
			}).then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.copyVirtualMachine = function(id) {
		console.log('virtualMachinesService.copyVirtualMachine() called with id ' + id);

		var deferred = $q.defer();

		$http({
			url: '../mocks/virtualMachines/copyVirtualMachineResponse.json',
			method: 'GET',
			cache: 'true',
			params: {
				id: id
			}
			}).then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.restoreVirtualMachine = function(id) {
		console.log('virtualMachinesService.restoreVirtualMachine() called with id ' + id);

		var deferred = $q.defer();

		$http({
			url: '../mocks/virtualMachines/restoreVirtualMachineResponse.json',
			method: 'GET',
			cache: 'true',
			params: {
				id: id
			}
			}).then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.destroyVirtualMachine = function(id) {
		console.log('virtualMachinesService.destroyVirtualMachine() called with id ' + id);

		var deferred = $q.defer();

		$http({
			url: '../mocks/virtualMachines/destroyVirtualMachineResponse.json',
			method: 'GET',
			cache: 'true',
			params: {
				id: id
			}
			}).then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.deleteSnapshot = function(snapshotID){
		console.log('virtualMachinesService.deleteSnapshot fired with ' + snapshotID);

		var deferred = $q.defer();

		$http({
			url: '../mocks/virtualMachines/deleteSnapshotResponse.json',
			method: 'GET',
			cache: 'true',
			params: {
				snapshotID: snapshotID
			}
			}).then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.revertVMToSnapshot = function(snapshotID){
		console.log('virtualMachinesService.revertVMToSnapshot fired with ' + snapshotID);

		var deferred = $q.defer();

		$http({
			url: '../mocks/virtualMachines/revertVMToSnapshotResponse.json',
			method: 'GET',
			cache: 'true',
			params: {
				snapshotID: snapshotID
			}
			}).then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.addNICToVM = function(nicID, vmID){
		console.log('virtualMachinesService.addNICToVM fired with ' + nicID, vmID);

		var deferred = $q.defer();

		$http({
			url: '../mocks/virtualMachines/addNICToVMResponse.json',
			method: 'GET',
			cache: 'true',
			params: {
				nicID: nicID,
				vmID: vmID
			}
			}).then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.updateDefaultNicForVM = function(nicID, vmID){
		console.log('virtualMachinesService.updateDefaultNicForVM fired with ' + nicID, vmID);

		var deferred = $q.defer();

		$http({
			url: '../mocks/virtualMachines/updateDefaultNicForVMResponse.json',
			method: 'GET',
			cache: 'true',
			params: {
				nicID: nicID,
				vmID: vmID
			}
			}).then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.removeNICFromVM = function(nicID, vmID){
		console.log('virtualMachinesService.removeNICFromVM fired with ' + nicID, vmID);

		var deferred = $q.defer();

		$http({
			url: '../mocks/virtualMachines/removeNICFromVMResponse.json',
			method: 'GET',
			cache: 'true',
			params: {
				nicID: nicID,
				vmID: vmID
			}
			}).then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.addIpToNic = function(nicID, ipAddress){
		console.log('virtualMachinesService.addIpToNic fired with ' + nicID, ipAddress);

		var deferred = $q.defer();

		$http({
			url: '../mocks/virtualMachines/addIpToNicResponse.json',
			method: 'GET',
			cache: 'true',
			params: {
				nicID: nicID,
				ipAddress: ipAddress
			}
			}).then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};

	virtualMachinesService.removeIpFromNic = function(secondaryIPAddressID){
		console.log('virtualMachinesService.removeIpFromNic fired with ' + secondaryIPAddressID);

		var deferred = $q.defer();

		$http({
			url: '../mocks/virtualMachines/removeIpFromNicResponse.json',
			method: 'GET',
			cache: 'true',
			params: {
				id: secondaryIPAddressID
			}
			}).then(function (response) {
				deferred.resolve(response);
			});

		console.log(deferred.promise);

		return deferred.promise;
	};
}]);