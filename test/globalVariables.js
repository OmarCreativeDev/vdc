var vdcApp = angular.module('vdcApp', [
							'ngAnimate',
							'ngCookies',
							'ngResource',
							'ngSanitize',
							'ngTouch',
							'ui.router',
							'pascalprecht.translate',
							'filterFromArray',
							'angularUtils.directives.dirPagination',
							'btford.socket-io'
						]);