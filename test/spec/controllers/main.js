'use strict';

describe('Controller: MainCtrl', function() {

	// load the controller's module
	beforeEach(module('vdcApp'));

	var MainCtrl,
		scope;

	// Initialize the controller and a mock scope
	beforeEach(inject(function($controller, $rootScope) {
		scope = $rootScope.$new();
		MainCtrl = $controller('MainCtrl', {
			$scope: scope
		});
	}));

	it('current language should be "en"', function() {
		expect(scope.currentLanguage).toBe('en');
	});
});