'use strict';

describe('Controller: VtoolsCtrl', function () {

  // load the controller's module
  beforeEach(module('vdcApp'));

  var VtoolsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    VtoolsCtrl = $controller('VtoolsCtrl', {
      $scope: scope
    });
  }));

  // it('should attach a list of awesomeThings to the scope', function () {
  //   expect(scope.awesomeThings.length).toBe(3);
  // });
});
