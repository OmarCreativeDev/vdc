'use strict';

describe('Controller: listVirtualMachines', function () {

  // load the controller's module
  beforeEach(module('vdcApp'));

  var listVirtualMachines,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    listVirtualMachines = $controller('listVirtualMachines', {
      $scope: scope
    });
  }));

  // it('should attach a list of awesomeThings to the scope', function () {
  //   expect(scope.awesomeThings.length).toBe(3);
  // });
});
