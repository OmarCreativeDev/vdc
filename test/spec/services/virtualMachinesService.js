'use strict';

describe('Service: virtualMachinesService', function () {

  // load the service's module
  beforeEach(module('vdcApp'));

  // instantiate service
  var virtualMachinesService;
  beforeEach(inject(function (_virtualMachinesService_) {
    virtualMachinesService = _virtualMachinesService_;
  }));

  it('should do something', function () {
    expect(!!virtualMachinesService).toBe(true);
  });

});
